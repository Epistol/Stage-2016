<!doctype html>
<html class="no-js" lang="fr">
<head>

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?php if(isset($varpage)){echo ucfirst($varpage). ' - DGFiP'; } else {echo 'DGFiP';} ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Place favicon.ico in the root directory -->

    <link rel="stylesheet" href="./css/main.css">
    <link rel="stylesheet" href="./css/pure.css">

      <link rel = "stylesheet" href = "https://code.jquery.com/ui/1.11.4/themes/ui-lightness/jquery-ui.css">
    <script src = "./js/html5shiv.js" > </script>
      <script src = "./js/jquery_ui/external/jquery/jquery.js" > </script>
  <script src = "./js/jquery-ui.js" > </script>

<script type="text/javascript">

   $(function(){
    $('.dateTxt').datepicker(); }
    ); 

    $.datepicker.setDefaults(
      {
          altField: "#datepicker",
          closeText: 'Fermer',
          prevText: 'Précédent',
          nextText: 'Suivant',
          currentText: 'Aujourd\'hui',
          monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
          monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
          dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
          dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
          dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
          weekHeader: 'Sem.',
          dateFormat: 'dd-mm-yy'
      }
  );

</script>

<script>
  function getState(val) {
    $.ajax({
    type: "POST",
    url: "get_state.php",
    data:'country_id='+val,
    success: function(data){
      $("#state-list").html(data);
    }
    });
  };

  function selectCountry(val) {
  $("#search-box").val(val);
  $("#suggesstion-box").hide();
  };
</script>



<script>
$(document).ready(function(){
  $("#report tr:odd").addClass("master");
  $("#report tr:not(.master)").hide();
  $("#report tr:first-child").show("slow" );
  $("#report tr.master").click(function(){
    $(this).toggleClass("master-clicked");
    $(this).next("tr").fadeToggle( "slow", "linear" );
    $(this).find(".arrow").toggleClass("up");
    }); 
  });
</script>


</head>
<body>


<div class="pure-g">

    <header class="pure-u-1 head_page">
        
            <!-- Title -->
            <div class="mdl-layout-title">
                <a href="./index.php" class="titre_header">
                    Intranet du suivi des formations
                </a>
            </div>
            
            <nav class="menu-haut">
                <ul>
                   
                   <?php 
                   if (!isset($_SESSION['user'])) {
                       echo  '<a class="navig_lien transition" href="./login.php"><li class="menu-li transition"><span class="transition">Connexion</span></li></a>';
                   }
                   else {
                       echo '<a class="navig_lien transition" href="./compte.php"><li class="menu-li transition"><span class="transition">Votre compte</span></li></a>';
                     echo '<a class="navig_lien transition" href="./logout.php"><li class="menu-li transition"><span class="transition">Déconnexion</span></li></a>';
                   }
                  
                ?>
                </ul>
            </nav>
        
    </header>
</div>


<div class="contenu_page">