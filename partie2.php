<?php



// ON MET UN NOM A NOTRE PAGE QUI EST DYNAMIQUEMENT INSERE DANS LE HEADER
$varpage = "Ajout formation";

//NOS PETITES FONCTIONS
require_once 'inc/config.php';

// LE HEADER
require_once 'inc/header.php';


// Vérification des variables

// si on viens bien de la page ajout_formation
if (isset($_POST['agent'])) {
	$agent = htmlspecialchars($_POST['agent']);
	$resultat = connect_table_where('agent','id',$agent);

	// si ddn est renseigné pour l'utilisateur choisi : 
		if (isset($resultat[0]['ddn'])) {
			list($year, $month, $day) = explode("-", $resultat[0]['ddn']);
		 	$ddn = "$day/$month/$year";
		}
		else {
			$ddn = "Pas de date de naissance renseigné";
		}
		// si nom est renseigné pour l'utilisateur choisi : 
		 if(isset($resultat[0]['nom'])){
		 	$nom = ucfirst($resultat[0]['nom']);
		 }
		 else {
		 	$nom = "Pas de nom renseigné";
		 }
		 // si prenom est renseigné pour l'utilisateur choisi : 
		 if(isset($resultat[0]['prenom'])){
		 	$prenom = ucfirst($resultat[0]['prenom']);
		 }
		 else {
		 	$prenom = "Pas de prénom renseigné";
		 }
		 // si mail est renseigné pour l'utilisateur choisi : 
		 if(isset($resultat[0]['mail'])){
		 	$mail = ucfirst($resultat[0]['mail']);
		 }
		 else {
		 	$mail = "Pas de mail renseigné";
		 }
		 // si service est renseigné pour l'utilisateur choisi : 

		 if(isset($resultat[0]['service'])){
		 	$service = ucfirst($resultat[0]['service']);
		 	// on fait une requete pour que l'id ne soit pas affiché et que le lib soit affiché à sa place
		 	$service2 = connect_table_where('service','id',$service);
		 	$service2 = $service2[0]['lib'];
		 }
		 else {
		 	$service2 = "Pas de service renseigné";
		 }




// Petit bloc d'infos sur l'agent à l'affichage.
	 
	echo '<div class="encart_agent">Demande de l\'agent : ' . $nom . ' ' . $prenom
	. '<br / > Email : <a href="mailto:' . $mail . '">' . $mail . '</a> <br /> Date de naissance : '. $ddn .
	'<br />N° DGFIP : ' . ucfirst($resultat[0]['id']) . '<br /> Service : ' . $service2 . '</div>';
	echo "Mauvais agent ? <a href='ajout_formation.php'> Revenez à la sélection </a>";
	
}


?>



<form class="pure-form pure-form-aligned" action="partie3.php" method="post"> 


<fieldset>
	<legend><u>Agent</u></legend>


	<fieldset>
		<legend>
			Action demandée
		</legend>
		<div class="pure-control-group"> <label for="code_action">Code de l'action</label>

		<select name="code_action" id="code_action">
				<?php 

				$formation = connect_table('formation');


				foreach ($formation as $row){
				echo '<option value="' . $row['id'] . '">' . ucfirst($row['type_formation']). ' - ' . ucfirst($row['nom_formation']) . '</option>';

				} ?>
				<option value="other">Autre</option>
			</select> </div>


		<div class="pure-control-group"> 
			<label for="fournisseur">Fournisseur :  </label>
			<select name="fournisseur" id="fournisseur">
				<?php 

				$fournisseur = connect_table('fournisseur');


				foreach ($fournisseur as $row){
				echo '<option value="' . $row['id'] . '">' . ucfirst($row['lib']). '</option>';

				} ?>
				<option value="other">Autre</option>
			</select>
		</div>


	</fieldset>


	<fieldset>
		<legend>
			Objectif du stagiaire
		</legend>

		 <textarea class="pure-input-1" placeholder="Attentes détaillées" name="attentes"></textarea> <br /><br />

		 <label >Si vous êtes concerné par une réforme de structures, avez-vous bénéficié : </label>

		 <label for="bilan_choix_1" class="pure-checkbox"> <input id="bilan_choix_1" name="bilan_choix1" type="checkbox" value="1"> d'un bilan préalable de vos connaissances théoriques ou pratiques </label> 
		 <label for="bilan_choix_2" class="pure-checkbox"> <input id="bilan_choix_2" name="bilan_choix2" type="checkbox" value="1"> d'un plan individuel de formation </label>

	</fieldset>


	<fieldset class="pure-group">
				


				 <!-- Partie date -->
				<legend>Préférences sur la date et la localisation de l'action</legend>

							<fieldset>
							<legend>Choix 1 : </legend>
							<fieldset class="pure-group"> 
								<div class="pure-control-group"> 
								 <input name="datedebut" id="datedebut" class="dateTxt" type="text" placeholder="Date de début"></div> 
										<div class="pure-control-group">   <input name="datefin" placeholder="Date de fin" id="datefin" class="dateTxt" type="text" >
								 </div> 
								<div class="pure-control-group"> <input id="datedefin_inconnu" name="datedefin_inconnu" type="text" placeholder="si inconnue : saisissez la période"> </div> 
							</fieldset>
								<div class="pure-control-group"> 
									<label  for="ville">Ville : </label> 
									<input name="ville" id="ville" type="text" placeholder="">
								</div> 
							


							<div class="pure-control-group">
								<label for="zone_inconnu">Si inconnue : </label>
								<input name="zone_inconnu" id="zone_inconnu" type="text" placeholder="saisissez le lieu"> 
							</div> 

							</fieldset>

							<fieldset>
							<legend>Choix 2 : </legend>
							<div class="pure-control-group"> 
							    <label for="datedebut2">Date de début : </label>  <input name="datedebut2" id="datedebut2" class="dateTxt" type="text" ></div> 
									<div class="pure-control-group">  <label for="datefin2">Date de fin : </label>  <input name="datefin2" id="datefin2" class="dateTxt" type="text" >
							 </div> 
							<div class="pure-control-group"> <label for="date_inconnu2">Si inconnue : </label> <input id="date_inconnu2" name="date_inconnu2" type="text" placeholder="saisissez la période"> </div> 
								<div class="pure-control-group"> 
									<label for="ville2">Ville : </label> 
									<input id="ville2" type="text" placeholder="">
								</div> 

							<div class="pure-control-group"> <label for="zone_inconnu2">Si inconnue : </label> <input name="zone_inconnu2" id="zone_inconnu2" type="text" placeholder="saisissez le lieu"> </div> 

							</fieldset>

					
				
	</fieldset>
	<fieldset>
		<legend>Demande de mobilisation du droit individuel à la formation (DIF)</legend>
		<div class="pure-control-group"> <label for="dif_choix">L'agent souhaite-t-il mobiliser son DIF ? </label> 
		 <label for="dif_1" class="pure-checkbox"> <input id="dif_1" name="dif_choix" type="radio" value="1">Oui </label> 
		 <label for="dif_2" class="pure-checkbox"> <input id="dif_2" name="dif_choix" type="radio" value="0">Non </label>


		</div>


		<div class="pure-control-group"> <label for="tps_travail">La formation au titre du DIF sera-t-elle suivie hors du travail ? </label> 
		 <label for="tps_travail1" class="pure-checkbox"> <input id="tps_travail1" name="tps_travail" type="radio" value="1">Oui </label> 
		 <label for="tps_travail2" class="pure-checkbox"> <input id="tps_travail2" name="tps_travail" type="radio" value="0">Non </label>


		</div>


	</fieldset>
	<fieldset>
		<legend>Date de demande : </legend>
		<div class="pure-control-group"> 
							    <label for="datedemande">Date de demande : </label>  <input name="datedemande" id="datedemande" class="dateTxt" type="text" ></div> 
	</fieldset>



</fieldset> 




	<fieldset> 
	<legend><u>Décision</u></legend>
		<fieldset>
			<div class="pure-control-group"> 

			<legend  for="qualif_t">Qualification de l'objectif de la formation par le chef de service : </legend>
				 <label for="qualif_t1" class="pure-checkbox"> <input id="qualif_t1" name="qualif_t" type="checkbox" value="1"> T1 </label> 
				 <label for="qualif_t2" class="pure-checkbox"> <input id="qualif_t2" name="qualif_t" type="checkbox" value="2"> T2 </label>
				 <label for="qualif_t3" class="pure-checkbox"> <input id="qualif_t3" name="qualif_t" type="checkbox" value="3"> T3 </label>


		</fieldset>
		<fieldset>
			<legend>Décision sur la demande de formation</legend>
			<div class="pure-control-group"> 

				<label for="decision_choix" >Avis du chef de service </label>
				<label for="decision_choix1" class="pure-checkbox"> <input id="decision_choix1" name="decision_choix" type="checkbox" value="1"> Acceptée </label> 
				<label for="decision_choix2" class="pure-checkbox"> <input id="decision_choix2" name="decision_choix" type="checkbox" value="0"> Refusée </label>

				<select name="motif_refus" id="motif_refus">
										<?php 

										$motifrefus  = array('Pas de refus', 'Stage non justifié par rapport à l\'activité de l\'agent', 'Stage non opportun au regard des nécéssités de service',
										'Candidature non prioritaire', 'Agent ne disposant pas du pré-requis', 'Nombre de candidats insuffisant pour ouvrir la session', 'Autre motif' );
										$i =0;
										foreach ($motifrefus as $row1){
											$i++;
										echo '<option value="' . $i . '">' . $row1. '</option>';

										} ?>
								</select> 

							</div>
							<div class="pure-control-group"> 
				<label for="decision_choix_dir" >Décision de la direction </label>
				<label for="decision_choix1_dir" class="pure-checkbox"> <input id="decision_choix1_dir" name="decision_choix_dir" 
				type="checkbox" value="1"> Acceptée </label>
				<label for="decision_choix2_dir" class="pure-checkbox"> <input id="decision_choix2_dir" name="decision_choix_dir" type="checkbox" value="2"> Refusée </label>

				<select name="motif_refus_dir" id="motif_refus_dir">
						<?php 
						$j = 0;
						$motifrefus  = array('Pas de refus', 'Stage non justifié par rapport à l\'activité de l\'agent', 'Stage non opportun au regard des nécéssités de service',
						'Candidature non prioritaire', 'Agent ne disposant pas du pré-requis', 'Nombre de candidats insuffisant pour ouvrir la session', 'Autre motif' );
						foreach ($motifrefus as $row1){
							$j++;
						echo '<option value="' . $j . '">' . $row1. '</option>';

						} ?>
				</select> 

			</div> 
		</fieldset>
	<input type="hidden" name="agent" value="<?php echo $agent; ?>">

	</fieldset>

	<button type="submit1" class="pure-button pure-button-primary">Envoyer</button>



</form>





<?php

// LE PIED DE PAGE
require_once 'inc/footer.php';
?>

