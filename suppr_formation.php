<?php 



// ON MET UN NOM A NOTRE PAGE QUI EST DYNAMIQUEMENT INSERE DANS LE HEADER
$varpage = "Suppresion de la formation en cours ...";


//NOS PETITES FONCTIONS
require_once 'inc/config.php';

// LE HEADER
require_once 'inc/header.php';


	if (isset($_GET['id'] )) {
		var_dump($_GET['id']);
		$id_form = $_GET['id'];

			if ($_SERVER['REQUEST_METHOD'] == 'POST') {
				if (isset($_POST['submit'])) {
					
				$stmt = $la_connexion->prepare("DELETE FROM formation WHERE id = :id");

				$stmt->bindParam(':id', $id_form);
				$stmt->execute();
				header('Location: consult_formation.php');


				}
				else if(isset($_POST['non'])){
					header('Location: consult_formation.php');
				}
			}

		
}



?>

<form class="pure-form pure-form-aligned" method="post">
	
	<div class="pure-controls">
				<p>Êtes-vous sur de vouloir supprimer la formation ?</p>

			<button type="submit" name="submit" class="pure-button pure-button-primary">Oui</button>
			<button type="submit" name="non" class="pure-button pure-button-primary">Non</button>
		</div>
</form>


<?php

// LE PIED DE PAGE
require_once 'inc/footer.php';
?>