<?php


// ON MET UN NOM A NOTRE PAGE QUI EST DYNAMIQUEMENT INSERE DANS LE HEADER
$varpage = "S'enregistrer";

//NOS PETITES FONCTIONS
require_once 'inc/config.php';

// LE HEADER
require_once 'inc/header.php';


		if ($_SERVER['REQUEST_METHOD'] == 'POST') {


			
			if(isset($_POST['nom'])){
				if (!empty($_POST['nom'])) {
					$nom =  htmlspecialchars($_POST['nom']);
					
				}
			}
			if(isset($_POST['service'])){
				if (!empty($_POST['service'])) {
					$service =  htmlspecialchars($_POST['service']);
					
				}
			}
			if(isset($_POST['qualification'])){
				if (!empty($_POST['qualification'])) {
					$qualification =  htmlspecialchars($_POST['qualification']);
					
				}
			}
			if(isset($_POST['profil'])){
				if (!empty($_POST['profil'])) {
					$profil =  htmlspecialchars($_POST['profil']);
					
				}
			}
			if(isset($_POST['grade'])){
				if (!empty($_POST['grade'])) {
					$grade =  htmlspecialchars($_POST['grade']);
					
				}
			}
			if(isset($_POST['dgfipid'])){
				if (!empty($_POST['dgfipid'])) {
					$dgfipid =  htmlspecialchars($_POST['dgfipid']);
					
				}
			}

				
			if(isset($_POST['prenom'])){
				if (!empty($_POST['prenom'])) {
					$prenom = htmlspecialchars($_POST['prenom']);
					}
			}
			if(isset($_POST['password'])){
				if (!empty($_POST['password'])) {
					$mdp = htmlspecialchars($_POST['password']);

					
					}
			}



		$stmt = $la_connexion->prepare("INSERT INTO agent (id,nom,prenom,mdp,service) VALUES (:id,:nom, :prenom, :mdp, :service)");
		$stmt->bindParam(':id', $dgfipid);
		$stmt->bindParam(':nom', $nom);
		$stmt->bindParam(':prenom', $prenom);
		$stmt->bindParam(':mdp', $mdp);
		$stmt->bindParam(':service', $service);
		$stmt->bindParam(':grade', $grade);
		$stmt->bindParam(':qualification', $qualification);
		$stmt->bindParam(':profil', $profil);
		$stmt->execute();


		}
	

// LE CONTENU :
?>




    <p>Enregistre chaque utilisateur </p>

	<form method="post" class="pure-form pure-form-aligned" action="<?php $_SERVER['PHP_SELF']?>">
    	<fieldset>
    		<div class="pure-control-group"> <label for="nom">ID DGFiP: </label> <input name ="dgfipid" id="dgfipid" type="text" required placeholder=""> </div>
    		<div class="pure-control-group"> <label for="nom">Votre nom : </label> <input name ="nom" id="nom" type="text" required placeholder=""> </div>
    		<div class="pure-control-group"> <label for="prenom">Votre prenom : </label> <input name ="prenom" id="prenom" type="text" required placeholder=""> </div>
    		<div class="pure-control-group"> <label for="password">Mot de passe : </label> <input id="password" name="password" type="password" required placeholder="">
    		 </div>
<div class="pure-control-group"> 
	<label for="service">Votre service : </label>
	<select name="service" id="service">
		<?php 

		$agent = connect_table('service');


		foreach ($agent as $row){
		echo '<option value="' . $row['id'] . '">' . ucfirst($row['lib']). '</option>';

		} ?>

	</select>
</div>
<div class="pure-control-group"> 
	<label for="qualification">Votre qualification : </label>
	<select name="qualification" id="qualification">
		<?php 

		$agent = connect_table('qualification');


		foreach ($agent as $row){
		echo '<option value="' . $row['id'] . '">' . ucfirst($row['lib']). '</option>';

		} ?>

	</select>
</div>
<div class="pure-control-group"> 
	<label for="grade">Votre grade : </label>
	<select name="grade" id="grade">
		<?php 

		$agent = connect_table('grade');


		foreach ($agent as $row){
		echo '<option value="' . $row['id'] . '">' . ucfirst($row['lib']). '</option>';

		} ?>

	</select>
</div>
<div class="pure-control-group"> 
	<label for="profil">Votre profil : </label>
	<select name="profil" id="profil">
		<?php 

		$agent = connect_table('profil');


		foreach ($agent as $row){
		echo '<option value="' . $row['id'] . '">' . ucfirst($row['lib']). '</option>';

		} ?>

	</select>
</div>


    		 <button type="submit" class="pure-button pure-button-primary">Envoyer</button>
    	</fieldset>
    </form>



<?php

// LE PIED DE PAGE
require_once 'inc/footer.php';
?>